# TIW-JDBC-Postgres

Description of the project and functionalities

## Material

* Group10_proggetazione.pptx: it must contain the design choices and architecture of the project
* init.sql: an sql script to inizilize the DB with the structure and test data
* TIW-JDBC-Postgres: the Eclipse project of TIW-JDBC but this time with Postgres
* [URL of the deployed project](https://tiw-jdbc.herokuapp.com/)

### Database URI
`postgres://oehhszrlmeaaaa:5b68f3cb2d000a1fc4894456a394ca03c8eeff6eca0472e4e8dee80188203a4f@ec2-53-222-78-30.eu-west-1.compute.amazonaws.com:5432/dkj5ijj3ospuk`

### Prerequisites

You need to have a Postgress database, Eclipse and Tomcat configured.

## Getting Started

To have this project up and running in your machine you need to complete the following steps:

1.  Import the database structure and test data from init.sql 
2.  Import project into Eclipse (File -> Import -> Existing project into workspace -> Choose project in your PC)
3.  Change username and password to work with your configuration (WebContent/WEB-INF/web.xml, change dbUrl, dbUser and dbPassword)
4.  Run the project (Run -> Run as... -> Run on Server, choose server configuration and Finish)

## Additional Information

### HREF inside your project
When you open the welcome page of this project you will see two sets of links "Heroku" and "Local", this is because when you run the project in your local machine you access like:

`localhost:8080/PROJECT_NAME`, 
while in Heroku it will be just:
`tiw-jdbc.herokuapp.com/`

For example if you have in your HTML pages something like this:
`<a href="/PROJECT_NAME/ConnectionTester">ConnectionTester</a>`
or like this:
`<a href="http://localhost:8080/PROJECT_NAME/ConnectionTester">ConnectionTester</a>`

You will need to modify it like this:
`<a href="/ConnectionTester">ConnectionTester</a>`

So for quick and easy test purposes in this case we replicated the links. This is something you need to have into account for your own project, but most likely if you use JSTL or Thymeleaf you won't encounter this issue, since your href will look like this:
`<a th:href="@{/Logout}">Logout</a>`

### .gitignore

Specifies intentionally untracked files to ignore. For example if you're a MAC OS user you need to ignore the .DS_STORE FILES

[More info](https://git-scm.com/docs/gitignore)

## Test accounts
| username | password |
| ------ | ------ |
| test1 | test1pass |
| test2 | test2pass | 

## Authors (group members)

* {person_code} Piero Fraternali 
* 10460101 - Federico Milani 
* 10589433 - Rocio Nahime Torres
