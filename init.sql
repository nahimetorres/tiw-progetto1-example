create table persons
(
    personid  serial not null
        constraint persons_pk
            primary key,
    firstname varchar(45) default NULL::character varying,
    lastname  varchar(45) default NULL::character varying,
    city      varchar(45) default NULL::character varying
);

create unique index persons_personid_uindex
    on persons (personid);

INSERT INTO public.persons (personid, firstname, lastname, city) VALUES (1, 'Piero', 'Fraternali', 'Como');
INSERT INTO public.persons (personid, firstname, lastname, city) VALUES (2, 'Nahime', 'Torres', 'La Plata');
INSERT INTO public.persons (personid, firstname, lastname, city) VALUES (3, 'Darian', 'Frajberg', 'La Plata');
INSERT INTO public.persons (personid, firstname, lastname, city) VALUES (4, 'Sergio', 'Herrera', 'Ciudad de Mexico');
INSERT INTO public.persons (personid, firstname, lastname, city) VALUES (5, 'Federico', 'Milani', 'Novara');
INSERT INTO public.persons (personid, firstname, lastname, city) VALUES (6, 'Marco', 'Brambilla', 'Como');
INSERT INTO public.persons (personid, firstname, lastname, city) VALUES (7, 'Fabio', 'Salice', 'Como');