package it.polimi.tiw.jdbctest;

import java.io.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

@WebServlet("/UnsafeQueryServlet")
public class UnsafeQueryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		String query = "SELECT * FROM persons WHERE personid = " + req.getParameter("ID"); // unsafe data inserted into
		Connection connection = null;
		ResultSet result = null;
		Statement statement = null;
		res.setContentType("text/plain");
		PrintWriter out = res.getWriter();
		try {
			connection = ConnectionHandler.getConnection(getServletContext());
			statement = connection.createStatement();
			result = statement.executeQuery(query);
			while (result.next()) {
				out.println("Firstname: " + result.getString("firstname") + " Lastname: " + result.getString("lastname")
						+ " City: " + result.getString("city"));
			}
		} catch (SQLException e) {
			out.append("SQL ERROR ");
		} finally {
			try {
				result.close();
			} catch (Exception e1) {
				out.append("SQL RES ERROR ");
			}
			try {
				statement.close();
			} catch (Exception e1) {
				out.append("SQL STMT ERROR");
			}
			ConnectionHandler.closeConnection(connection);
		}
	}

}