package it.polimi.tiw.jdbctest;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/PreparedQueryServlet")
public class PreparedQueryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		Connection connection = null;
		String city = req.getParameter("city"); // nullity test omitted..
		String query = "SELECT * FROM persons WHERE city = ?";
		ResultSet result = null;
		PreparedStatement pstatement = null;
		res.setContentType("text/plain");
		PrintWriter out = res.getWriter();
		try {
			connection = ConnectionHandler.getConnection(getServletContext());
			pstatement = connection.prepareStatement(query);
			pstatement.setString(1, city);
			result = pstatement.executeQuery();

			while (result.next()) {
				out.println("Firstname: " + result.getString("firstname") + " Lastname: " + result.getString("lastname")
						+ " City: " + result.getString("city"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			out.append("SQL ERROR");
		} finally {
			try {
				result.close();
			} catch (Exception e1) {
				e1.printStackTrace();
				out.append("SQL RES ERROR");
			}
			try {
				pstatement.close();
			} catch (Exception e1) {
				e1.printStackTrace();
				out.append("SQL STMT ERROR");
			}
			ConnectionHandler.closeConnection(connection);
		}
	}

}