package it.polimi.tiw.jdbctest;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/HTMLQueryServlet")
public class HTMLQueryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		Connection connection = null;
		String query = "SELECT firstname, lastname, city FROM persons";
		ResultSet result = null;
		Statement statement = null;
		res.setContentType("text/html");
		PrintWriter out = res.getWriter();
		try {
			connection = ConnectionHandler.getConnection(getServletContext());
			statement = connection.createStatement();
			result = statement.executeQuery(query);
			out.println("<html>\n" + "<head>\n" + "<title>Example of servlet accessing database</title>\n" + "</head>\n"
					+ "<body>\n" + "<h2>Example of servlet accessing database</h2>\n" + "<table border=1>\n" + "<tr>\n"
					+ "<th>Firstname</th>\n" + "<th>Lastname</th>\n" + "<th>City</th>\n" + "</tr>\n" + "<tr>\n");
			while (result.next()) {
				out.println(
						"<tr><td>" + result.getString("FirstName") + "</td>\n" + "<td>" + result.getString("LastName")
								+ "</td>\n" + "<td>" + result.getString("City") + "</td></tr>\n");
			}
			out.println("</tr>\n" + "</table>\n" + "</body>\n");
		} catch (SQLException e) {
			e.printStackTrace();
			out.append("SQL ERROR");
		} finally {
			try {
				result.close();
			} catch (Exception e1) {
				e1.printStackTrace();
				out.append("SQL RES ERROR");
			}
			try {
				statement.close();
			} catch (Exception e1) {
				e1.printStackTrace();
				out.append("SQL STMT ERROR");
			}
			ConnectionHandler.closeConnection(connection);
		}
	}
}