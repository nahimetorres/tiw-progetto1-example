package it.polimi.tiw.jdbctest;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/BasicQueryServlet")
public class BasicQueryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		Connection connection = null;
		String query = "SELECT firstname, lastname, city FROM persons";
		ResultSet result = null;
		Statement statement = null;
		res.setContentType("text/plain");
		PrintWriter out = res.getWriter();
		try {
			connection = ConnectionHandler.getConnection(getServletContext());
			statement = connection.createStatement();
			result = statement.executeQuery(query);
			while (result.next()) {
				out.println("Firstname: " + result.getString("firstname") + " Lastname: " + result.getString("lastname")
						+ " City: " + result.getString("city"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			out.append("SQL ERROR");
		} finally {
			try {
				result.close();
			} catch (Exception e1) {
				e1.printStackTrace();
				out.append("SQL RES ERROR");
			}
			try {
				statement.close();
			} catch (Exception e1) {
				e1.printStackTrace();
				out.append("SQL STMT ERROR");
			}
			ConnectionHandler.closeConnection(connection);
		}
	}

}